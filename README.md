# Pyspark Course

## Objetivo
Deixar um cientista de dados com zero ou pouca experiência com Spark (e SQL) confortável com essa tecnologia. Os conteúdos abordados vão desde princípios básicos (sobre o uso da operação filter, por exemplo) até conceitos avançados (como otimização de queries) e feature engineering, tudo embasado por exemplos práticos de sua utilização e impacto no desenvolvimento dos projetos.

## Aula 1 - Conceitos básicos da API Python para Spark
- Ambiente Databricks
- Revisão sobre SQL:
- Conceitos básicos e sua conexão com o curso;
- Leitura de arquivos
- Principais métodos (e exemplos de uso) para uso no dia-a-dia:
    - Leitura de arquivos;
    - Filtros;
    - Agrupamentos;
    - Janelas;
    - Salvamento de arquivos;
- UDFs

## Aula 2 - Construção de bases de dados com Spark

## Aula 3 - Conceitos de para Construção de Pipelines Produtivos
